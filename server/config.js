var nconf = require("nconf");

var env = 'development';

if (process.env.NODE_ENV || process.env.NODE_ENV!=='development') {
    env = 'production';
}

nconf
    .argv()
    .env()
    .file({
        file : __dirname+'\\_'+env+'.json'
    })
;

module.exports = nconf;