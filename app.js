// Libraries
var express = require("express"), // framework
    io = require("socket.io"), // hot req and res between client and server
    async = require("async"), // async operations
    fs = require("fs"), // work with files
    jade = require("jade"), // jade html preprocessor
    less = require("less"),  // less css preprocessor
    logger = require("morgan"), // logger
    favicon = require("favicon"), // favicon set
    methodOverride = require("method-override"), // support of 'delete' & 'put' methods
    cookieParser = require("cookie-parser"), // cookies parse
    bodyParser = require("body-parser"), // processing application/x-www-form-urlencoded & application/json
    path = require("path"), // work with path
    util = require("util"); // some additional tools like inherits and other

// Settings
process.env.NODE_ENV = "production"; //type of management. Development / Production.
var config = require('./server/config.js'),
    app = express(); // express management

app.set('views', path.join(__dirname, "client/templates")); //directory of jade files
app.set("view engine", 'jade'); //jade engine template

// Set middlewares to app
//app.use(favicon());
app.use(methodOverride());
app.use(logger("dev"));
app.use(cookieParser());
app.use(bodyParser());
app.use(express.static(__dirname + "/client/public")); //public directory
// Less rendering. Must be first.
app.get("*.less", function(req, res, next) {
    var pathname = path.join(__dirname, "client", req.url); //set "client" as first input directory of less files
    fs.exists(pathname, function(exists) {
        if (exists) {
            fs.readFile(pathname, "utf8", function(err, data) {
                if (err) throw err;
                less.render(data, function(err, css) {
                    if (err) throw err;
                    res.set("Content-type", "text/css");
                    res.send(css);
                });
            });
        } else {
            next();
        }
    });
});

// All route patches
app.get("*", function(req,res) {
    res.render('page', {
        title : 'home'
    });
});

console.log(config);

app.listen(config.get('http:port'), function() {
    console.log("Server in "+process.env.NODE_ENV+" mode\n"+ "HOST: " + config.get('http:host') + "\nPORT : " + config.get('http:port') +"\nStarting at "+new Date());
});